package com.zuitt.example.Application.services;

import com.zuitt.example.Application.models.User;
import com.zuitt.example.Application.repositories.UserRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private UserRepositories userRepositories;
    public void createUser(User user){
        userRepositories.save(user);

    }
     public Optional<User> findByUsername(String username){
        return Optional.ofNullable(userRepositories.findByUsername(username));
     }

}
