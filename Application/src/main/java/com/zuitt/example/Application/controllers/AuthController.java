package com.zuitt.example.Application.controllers;


import com.zuitt.example.Application.exceptions.UserException;
import com.zuitt.example.Application.models.JwtRequest;
import com.zuitt.example.Application.models.JwtResponse;
import com.zuitt.example.Application.models.User;
import  com.zuitt.example.Application.services.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin
public class AuthController {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private  JwtToken jwtToken;

    @Autowired
    private  JwtUserDetailsService jwtUserDetailsService;

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest autheticationRequest) throws Exception{


            authenticate(autheticationRequest.getUsername(), autheticationRequest.getPassword());

            final UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(autheticationRequest.getUsername());

            final  String token = jwtToken.generateToken(userDetails);

            return ResponseEntity.ok(new JwtResponse(token));

    }
    private void authenticate(String username, String password) throws Exception{
        try{
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        }catch (DisabledException e){
            throw new Exception("USER_DISABLED", e);
        }catch (BadCredentialsException e){
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
    @RequestMapping(value = "/users/register", method = RequestMethod.POST)
    public ResponseEntity<Object> register(@RequestBody Map<String, String> body) throws UserException{
        String username = body.get("username");
        if (!userService.findByUsername(username).isEmpty()){
            throw  new UserException("Username already exist");
        }else{
            String password = body.get("password");
            String encodedPassword = new BCryptPasswordEncoder().encode(password);
            User newUser = new User(username, encodedPassword);
            userService.createUser(newUser);

            return  new ResponseEntity<>("User Register Succesfully", HttpStatus.CREATED)
        }
    }
}
