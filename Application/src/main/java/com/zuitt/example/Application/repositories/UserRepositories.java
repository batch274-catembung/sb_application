package com.zuitt.example.Application.repositories;


import com.zuitt.example.Application.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepositories extends CrudRepository<User, Object> {
 User findByUsername(String username);


}